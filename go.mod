module gitlab.com/okotek/okostamp

go 1.17

require (
	gitlab.com/okotek/okoframe v0.0.0-20211106035311-149369c29e05
	gitlab.com/okotek/okolog v0.0.0-20211111020849-a95cfef9a1bb
	gitlab.com/okotek/okonet v0.0.0-20211106032734-e4b07b8b93fb
	gocv.io/x/gocv v0.29.0
)

replace gitlab.com/okotek/okoframe => ../okoframe

replace gitlab.com/okotek/okolog => ../okolog

replace gitlab.com/okotek/okonet => ../okonet
