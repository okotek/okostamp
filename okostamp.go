package main

import (
	"bufio"
	"flag"
	"fmt"
	"image"
	"image/color"
	"log"
	"os"

	"gitlab.com/okotek/okoframe"
	"gitlab.com/okotek/okolog"
	"gitlab.com/okotek/okonet"

	"gocv.io/x/gocv"
)

func main() {

	sendoffTarget := flag.String("sendoffTarget", "localhost:8083", "Sendoff target.")
	recieverTarget := flag.String("recieverTarget", ":8082", "Port to listen for incoming on.")
	flag.Parse()

	stamperCount := 5

	//cfig := loadOptions.Load()
	//okolog.LoggerIDInit(cfig)
	logInit := okolog.SetLogPreDat("localhost:8066", "localhost:8066", "master")

	//cfig.InstanceIdentifier = okolog.InitStruct.Identifier

	stageOneChan := make(chan okoframe.Frame)
	stageTwoChan := make(chan okoframe.Frame)

	go okonet.SendoffHandlerCached(stageTwoChan, *sendoffTarget, 25)
	go okonet.ReceiverHandler(stageOneChan, *recieverTarget)
	for iter := 0; iter < stamperCount; iter++ {
		go stamper(stageOneChan, stageTwoChan, logInit)
	}

	//Prevent program from quitting
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func stamper(input chan okoframe.Frame, output chan okoframe.Frame, init okolog.LogPreDat) {

	offWhite := color.RGBA{200, 200, 200, 250}

	for iter := range input {
		img, err := gocv.NewMatFromBytes(
			iter.Height,
			iter.Width,
			iter.Type,
			iter.ImDat,
		)

		if err != nil {
			log.Printf("Error in stamper: ", err)
		}

		//Iterate over all detections in the image and draw boxes on them.
		//This was copied from a website for shame
		for _, detection := range iter.ClassifierTags {

			if len(detection.Rects) > 0 {
				for _, rct := range detection.Rects {
					gocv.Rectangle(&img, rct, offWhite, 3)
					size := gocv.GetTextSize(detection.ClassName, gocv.FontHersheyPlain, 1.2, 2)
					pt := image.Pt(rct.Min.X+(rct.Min.X/2)-(size.X/2), rct.Min.Y-2)
					gocv.PutText(&img, detection.ClassName, pt, gocv.FontHersheyPlain, 1.2, offWhite, 2)
				}
			} else {
				fmt.Println("Zero len rectangle list")
				continue
			}
		}

		outputImg := okoframe.CreateFrameFromExisting(img,
			iter.UserName,
			iter.UserPass,
			iter.ClassifierTags,
			iter.MotionPercentChange,
			iter.ImTime,
			iter.CamID,
			iter.UnitID,
			iter.UnitID)

		/*
			go func() {

				select {
				case output <- outputImg:
					//fmt.Println("Channel send worked.")
				default:
					fmt.Println("Channel send failed.")
					output <- outputImg
				}
			}()

			go func() {
				ed := iter
				ed.ImgName = ed.ImgName + ";no_rectangles"
				select {
				case output <- ed:
					//fmt.Println("Channel send worked.")
				default:
					fmt.Println("Channel send failed.")
					output <- ed
				}
			}()
		*/

		ed := iter
		ed.ImgName = ed.ImgName + ";no_rectanges"
		output <- ed
		output <- outputImg

		okolog.MessageSendoff(init, "Stamped new image.")

	}
}

func getDetCount(input okoframe.Frame) int {
	detectionCount := 0
	for _, iter := range input.ClassifierTags {
		if len(iter.Rects) < 1 {
			fmt.Println("Nothing there. Dump this")
			continue
		}
		detectionCount++
	}
	return detectionCount
}
